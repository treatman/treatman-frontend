import {defineConfig} from 'vite'
import Vue from '@vitejs/plugin-vue'
import {imagetools} from 'vite-imagetools'
import ImageMin from 'vite-plugin-imagemin'
import Pages from "vite-plugin-pages";
import ViteComponents from 'vite-plugin-components'

// https://vitejs.dev/config/
export default defineConfig({
    server: {
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, '')
            }
        }
    },
    plugins: [
        Vue({
            include: [/\.vue$/],
        }),
        imagetools(),
        Pages({
            pagesDir: ['src/pages'],
            extendRoute(route, parent) {
                if (route.path === "login") {
                    // Login is unauthenticated.
                    return {
                        ...route,
                        meta: { auth: false },
                    };
                }

                // Augment the route with meta that indicates that the route requires authentication.
                return {
                    ...route,
                    meta: { auth: true },
                };
            },
        }),
        ViteComponents({
            extensions: ['vue', 'md'],
            dirs: ['documentation', 'src/components', 'src/layouts'],
            customLoaderMatcher: (path) => path.endsWith('.md'),
        }),
        ImageMin({
            gifsicle: {
                optimizationLevel: 7,
                interlaced: false,
            },
            optipng: {
                optimizationLevel: 7,
            },
            mozjpeg: {
                quality: 8,
            },
            pngquant: {
                quality: [0.8, 0.9],
                speed: 4,
            },
            svgo: {
                plugins: [
                    {
                        removeViewBox: false,
                    },
                    {
                        removeEmptyAttrs: false,
                    },
                ],
            },
        }),
    ]
})
