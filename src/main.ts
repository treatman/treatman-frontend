import {createApp} from 'vue'
import App from './App.vue'
import router from "./router";

import Harlem from '@harlem/core';
import createStoragePlugin from '@harlem/plugin-storage';

import './scss/main.scss'

const storagePlugin = createStoragePlugin('*', {
    type: 'local',
    prefix: 'treatman',
    sync: true
});

const app = createApp(App)

app.use(router)
app.use(Harlem, {
    plugins: [storagePlugin]
})
app.mount('#app')
