import {
    createStore
} from '@harlem/core';
import {UserData} from "../services/dto/UserData";

interface State {
    userData?: UserData;
    authenticated?: boolean;
    token?: string;
}

export const STATE: State = {
    userData: new UserData(),
    authenticated: false,
    token: ''
};

const {
    getter,
    mutation,
    ...store
} = createStore<State>('user', STATE);

export function getState() {
    const storedState = localStorage.getItem('treatman:user');

    if (!storedState) {
        return STATE;
    }

    const state = JSON.parse(storedState);

    return {
        ...STATE,
        ...state
    };
}

export const setUserData = mutation<UserData>('setUserData', (state, payload) => {
    state.userData = payload;
});

export const setToken = mutation<string>('setToken', (state, payload) => {
    state.token = payload;
});

export const setAuthenticated = mutation<boolean>('setAuthenticated', (state, payload) => {
    state.authenticated = payload;
});
