import {createRouter, createWebHistory} from 'vue-router'

// @ts-ignore
import routes from 'pages-generated'

import {
    getState,
} from './stores/users';

const router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach((to, from) => {
    if (to.meta.auth !== undefined && to.meta.auth && !getState().authenticated) {
        return {
            name: "auth-Login",
            query: {redirect: to.fullPath}
        }
    }
})

export default router
