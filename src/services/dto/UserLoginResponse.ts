export class UserLoginResponse {
    public message: string;
    public token: string;

    constructor(message: string, token: string) {
        this.message = message;
        this.token = token;
    }
}
