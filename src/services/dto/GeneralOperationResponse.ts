export class GeneralOperationResponse {
    public message: string | undefined;
    public success: boolean | undefined;
}
