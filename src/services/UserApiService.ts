import http from "../http-common";
import {UserLoginResponse} from "./dto/UserLoginResponse";
import {UserLoginRequest} from "./dto/UserLoginRequest";
import {AxiosResponse} from "axios";

import {
    STATE
} from '../stores/users';
import {GeneralOperationResponse} from "./dto/GeneralOperationResponse";
import {UserCreateRequest} from "./dto/UserCreateRequest";

class UserApiService {

    public login(data: UserLoginRequest): Promise<AxiosResponse<UserLoginResponse>> {
        // @ts-ignore
        return new http.post("/api/login", data);
    }

    public createUser(data: UserCreateRequest): Promise<AxiosResponse<GeneralOperationResponse>> {
        // @ts-ignore
        return new http.post("/api/users/createUser", data, {
            headers: {'Authorization': 'Bearer ' + STATE.token}
        });
    }

}

export default new UserApiService();
